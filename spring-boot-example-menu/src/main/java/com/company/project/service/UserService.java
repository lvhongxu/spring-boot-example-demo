package com.company.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.company.project.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author pzblog
 * @since 2020-06-28
 */
public interface UserService extends IService<User> {

}
