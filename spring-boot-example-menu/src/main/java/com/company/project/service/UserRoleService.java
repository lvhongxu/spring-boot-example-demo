package com.company.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.company.project.entity.UserRole;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author pzblog
 * @since 2020-06-28
 */
public interface UserRoleService extends IService<UserRole> {

}
