package com.company.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.company.project.entity.UserRole;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author pzblog
 * @since 2020-06-28
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
