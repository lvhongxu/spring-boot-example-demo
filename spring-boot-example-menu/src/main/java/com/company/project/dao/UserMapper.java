package com.company.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.company.project.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author pzblog
 * @since 2020-06-28
 */
public interface UserMapper extends BaseMapper<User> {

}
