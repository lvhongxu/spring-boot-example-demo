package com.example.smail.entity;

/**
 * <p>
 * Mail
 * </p>
 *
 * @author panzhi
 * @since 2024/7/8
 */
public class Mail {

    /**
     * 目标邮箱地址
     */
    private String to;

    /**
     * 标题不能为空
     */
    private String title;

    /**
     * 正文不能为空
     */
    private String content;

    /**
     * 消息id
     */
    private String msgId;

    public String getTo() {
        return to;
    }

    public Mail setTo(String to) {
        this.to = to;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Mail setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Mail setContent(String content) {
        this.content = content;
        return this;
    }

    public String getMsgId() {
        return msgId;
    }

    public Mail setMsgId(String msgId) {
        this.msgId = msgId;
        return this;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "to='" + to + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", msgId='" + msgId + '\'' +
                '}';
    }
}
