package com.example.chapter.config;

/**
 * <p>
 * WebMvcConfig
 * </p>
 *
 * @author panzhi
 * @since 2024/6/16
 */
//@Configuration
//public class WebMvcConfig implements WebMvcConfigurer {
//
//
//    /**
//     * 与CrossFilter作用相同，二选一
//     * @param registry
//     */
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowedOrigins("*")
//                .allowedMethods("GET", "POST", "PUT", "DELETE")
//                .maxAge(3600)
//                .allowedHeaders("Origin", "Accept", "Content-Type", "Authorization")
//                .allowCredentials(true);
//    }
//}